DHC-2 Beaver castering tailwheel mod
====================================

Mod for DHC-2 Beaver in Microsoft Flight Simulator for realistic steering on
the ground using only differential braking or rudder control surfaces at
higher speeds.

Meant to be used together with [Tiger's DHC-2 Beaver Mod v1.2][1].

[1]: https://www.flightsim.com/files/file/210178-tigers-dhc-2-beaver-mod-v12/ "Tiger's DHC-2 Beaver Mod v1.2"


Locations
---------

[Project page][2] is hosted on GitLab.

Ready to use packages can be downloaded from the project's [releases page][3].

If you find a problem with this mod, you can [open an issue][4] on the project page.

[2]: https://gitlab.com/randolf.lemmont/fs-dhc2-castering-mod "Project page"
[3]: https://gitlab.com/randolf.lemmont/fs-dhc2-castering-mod/-/releases "Releases"
[4]: https://gitlab.com/randolf.lemmont/fs-dhc2-castering-mod/-/issues "Issues"


Install
-------

Download a [release zip package][5], unpack the zip archive, place the
folder `x-randolf-castering-dhc2` into your MSFS Community folder and (re)start
the simulator.

[5]: https://gitlab.com/randolf.lemmont/flightsim-checklist-tools/-/releases "Releases"

